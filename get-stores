#!/usr/bin/env python3

import sys
sys.exit(0)

import requests
import demjson
import geojson
import re
import xml.etree.ElementTree as ET
import platform, datetime
import pprint

def record_stats(item, value, n=1):
  if item not in stats:
    stats[item] = {}
  if value not in stats[item]:
    stats[item][value] = 0
  stats[item][value] += n


product_url_pattern = 'https://www.ikea.com/{}/p/-{}/'

product_js_pattern = r'"(/{}/(?:products/)?javascripts/(?:range-pip-)?main\.[^"]+\.js)"'

stock_check_js_pattern = r'(\d+:)"((?:range|owi)-stock-?check)".*\1"([^"]+)"'

stores_url_patterns = [
  'https://www.ikea.com/{}/products/javascripts/{}.{}.js',
  'https://www.ikea.com/{}/javascripts/{}.{}.js',
]
stores_obj_patterns = [
  r'(?:stores:|allStores=)(\[[^\]].*}}]),',
  r'(?:stores:|allStores=)(\[[^\]].*?}]),',
  r'(?:stores:|allStores=)(\[[^\]].*}]),',
]

xml_url_pattern = 'https://www.ikea.com/{}/iows/catalog/availability/{}/'

stock_url_pattern = 'https://iows.ikea.com/retail/iows/{}/stores/{}/availability/ART/{}'
stock_json_headers = {'accept': 'application/vnd.ikea.iows+json;version=1.0', 'contract': '37249', 'consumer': 'MAMMUT'}

countries = [
  {'code': 'de/de', 'api': 'json+xml',   'products': {'Blåhaj': '30373588', 'Smolhaj': '50455234'}}, # 53 stores
  {'code': 'us/en', 'api': 'json+xml',   'products': {'Blåhaj': '90373590', 'Smolhaj': '00455236'}}, # 50 stores
  {'code': 'fr/fr', 'api': 'json+xml',   'products': {'Blåhaj': '30373588', 'Smolhaj': '50455234'}}, # 34 stores
  {'code': 'gb/en', 'api': 'json+xml',   'products': {'Blåhaj': '30373588', 'Smolhaj': '50455234'}}, # 22 stores
  {'code': 'it/it', 'api': 'json+xml',   'products': {'Blåhaj': '30373588', 'Smolhaj': '50455234'}}, # 21 stores
  {'code': 'se/sv', 'api': 'json+xml',   'products': {'Blåhaj': '30373588', 'Smolhaj': '50455234'}}, # 20 stores
  {'code': 'ru/ru', 'api': 'json+xml',   'products': {'Blåhaj': '40373597', 'Smolhaj': '80455237'}}, # 17 stores, no smolhaj (404)
  {'code': 'es/es', 'api': 'json+xml',   'products': {'Blåhaj': '30373588', 'Smolhaj': '50455234'}}, # 16 stores
  {'code': 'ca/en', 'api': 'json+xml',   'products': {'Blåhaj': '90373590', 'Smolhaj': '00455236'}}, # 14 stores
  {'code': 'nl/nl', 'api': 'json+xml',   'products': {'Blåhaj': '30373588', 'Smolhaj': '50455234'}}, # 13 stores
  {'code': 'pl/pl', 'api': 'json+xml',   'products': {'Blåhaj': '30373588', 'Smolhaj': '50455234'}}, # 10 stores
  {'code': 'au/en', 'api': 'json+xml',   'products': {'Blåhaj': '10373589', 'Smolhaj': '20455235'}}, # 10 stores
  {'code': 'ch/en', 'api': 'json+xml',   'products': {'Blåhaj': '30373588', 'Smolhaj': '50455234'}}, #  9 stores
  {'code': 'be/nl', 'api': 'json+xml',   'products': {'Blåhaj': '30373588', 'Smolhaj': '50455234'}}, #  8 stores
  {'code': 'at/de', 'api': 'json+xml',   'products': {'Blåhaj': '30373588', 'Smolhaj': '50455234'}}, #  7 stores
  {'code': 'no/no', 'api': 'json+xml',   'products': {'Blåhaj': '30373588', 'Smolhaj': '50455234'}}, #  7 stores
  {'code': 'dk/da', 'api': 'json+xml',   'products': {'Blåhaj': '30373588', 'Smolhaj': '50455234'}}, #  6 stores, no smolhaj (404)
  {'code': 'fi/fi', 'api': 'json+xml',   'products': {'Blåhaj': '30373588', 'Smolhaj': '50455234'}}, #  5 stores
  {'code': 'pt/pt', 'api': 'json+xml',   'products': {'Blåhaj': '30373588', 'Smolhaj': '50455234'}}, #  5 stores
  {'code': 'sa/en', 'api': 'json+xml',   'products': {'Blåhaj': '30373588', 'Smolhaj': '50455234'}}, #  4 stores
  {'code': 'my/en', 'api': 'json+xml',   'products': {'Blåhaj': '10373589', 'Smolhaj': '20455235'}}, #  4 stores
  {'code': 'ae/en', 'api': 'json+xml',   'products': {'Blåhaj': '30373588', 'Smolhaj': '50455234'}}, #  3 stores
  {'code': 'sg/en', 'api': 'json+xml',   'products': {'Blåhaj': '10373589', 'Smolhaj': '20455235'}}, #  2 stores
  {'code': 'ro/ro', 'api': 'json+xml',   'products': {'Blåhaj': '30373588', 'Smolhaj': '50455234'}}, #  2 stores
  {'code': 'th/en', 'api': 'json+xml',   'products': {'Blåhaj': '10373589', 'Smolhaj': '20455235'}}, #  2 stores
  {'code': 'kw/en', 'api': 'json+xml',   'products': {'Blåhaj': '30373588', 'Smolhaj': '50455234'}}, #  2 stores
  {'code': 'rs/sr', 'api': 'json+xml',   'products': {'Blåhaj': '30373588', 'Smolhaj': '50455234'}}, #  1 store
  {'code': 'ie/en', 'api': 'json+xml',   'products': {'Blåhaj': '30373588', 'Smolhaj': '50455234'}}, #  1 store
  {'code': 'eg/en', 'api': 'json+xml',   'products': {'Blåhaj': '30373588', 'Smolhaj': '50455234'}}, #  1 store
  {'code': 'qa/en', 'api': 'json+xml',   'products': {'Blåhaj': '30373588', 'Smolhaj': '50455234'}}, #  1 store
  {'code': 'hr/hr', 'api': 'json+xml',   'products': {'Blåhaj': '30373588', 'Smolhaj': '50455234'}}, #  1 store, no smolhaj (404)
#  {'code': 'bh/en', 'api': 'json+json',  'products': {'Blåhaj': '30373588', 'Smolhaj': '50455234'}}, #  1 store
#  {'code': 'in/en', 'api': 'json+json',  'products': {'Blåhaj': '10373589', 'Smolhaj': '20455235'}}, #  1 store
]

all_stores = {}

stats = {}

for country in countries:
  print('Scraping {} using {}...'.format(country['code'], country['api']))
  if country['api'].startswith('json+'):
    product_url = product_url_pattern.format(country['code'], list(country['products'].values())[0])
    product_page = requests.get(product_url)

    main_js_url = 'https://www.ikea.com'+re.search(product_js_pattern.format(country['code']), product_page.text).group(1)
    main_js = requests.get(main_js_url)

    stock_check_re = re.search(stock_check_js_pattern, main_js.text)
    js_name = stock_check_re.group(2)

    record_stats('js_name', js_name)

    print(' - using {}'.format(js_name))

    for stores_url_pattern in stores_url_patterns:
      stock_check_url = stores_url_pattern.format(country['code'], js_name, stock_check_re.group(3))
      print(' - trying url %s' % stock_check_url)
      stores_js = requests.get(stock_check_url)
      if stores_js.status_code == 200:
        record_stats('stores_url_pattern', stores_url_pattern)
        break
    else:
      print(' - [!!!] Failed all store url patterns')
      #sys.exit(42)
      continue

    for stores_obj_pattern in stores_obj_patterns:
      print(' - trying regex %s' % stores_obj_pattern)
      try:
        stores_obj_str = re.search(stores_obj_pattern, stores_js.text).group(1)
        json_stores = demjson.decode(stores_obj_str)
        record_stats('stores_obj_pattern', stores_obj_pattern)
        break
      except demjson.JSONDecodeError:
        continue
      except AttributeError:
        continue
    else:
      print(' - [!!!] Failed all regexes')
      #sys.exit(42)
      continue

    for store in json_stores:
      if 'storeLocation' in store.keys():
        all_stores[store['value']] = {}
        all_stores[store['value']]['name'] = store['name']
        all_stores[store['value']]['lat'] = store['storeLocation']['latitude']
        all_stores[store['value']]['lng'] = store['storeLocation']['longitude']
        all_stores[store['value']]['stock'] = {}
      else:
        print(' ! [!!!] IKEA {} missing storeLocation in JSON, skipping...'.format(store['name']))
  elif country['api'].startswith('aujson+'):
    au_stores = requests.get('https://www.ikea.com/ms/en_AU/secure/data/location.json').json()
    for store in au_stores:
      all_stores[store['id']] = {}
      all_stores[store['id']]['name'] = store['name']
      all_stores[store['id']]['lat'] = store['lat']
      all_stores[store['id']]['lng'] = store['lng']
      all_stores[store['id']]['stock'] = {}

  for product in country['products'].items():
    if country['api'].endswith('+xml'):
      xml_url = xml_url_pattern.format(country['code'], product[1])
      xml_page = requests.get(xml_url)
      if xml_page.status_code >= 400:
        print(' ! [xml] HTTP {} for {} ({}), url was {}'.format(xml_page.status_code, product[0], product[1], xml_url))
        continue

      xml_root = ET.fromstring(xml_page.text)
      xml_stores = xml_root.find('availability')
      print(' - {} IKEA\'s for {} ({})'.format(len(xml_stores), product[0], product[1]))

      for store in xml_stores:
        stock_num = 0
        stock = store.find('stock').find('availableStock')
        if stock is not None:
          stock_num = int(stock.text)
        if store.get('buCode') not in all_stores:
          print(' ! [!!!] IKEA #{} in {} does exist in XML but not in JSON. {} {}s got lost.'.format(store.get('buCode'), country['code'], stock_num, product[0]))
          continue
        all_stores[store.get('buCode')]['stock'][product[0]] = stock_num
    elif country['api'].endswith('+json'):
      print(' - {} IKEA\'s for {} ({})'.format(len(json_stores), product[0], product[1]))

      for store in json_stores:
        json_url = stock_url_pattern.format(country['code'], store['value'], product[1])
        stock_json = requests.get(json_url, headers=stock_json_headers)
        if stock_json.status_code >= 400:
          print(' ! [json] HTTP {} for {}, url was {}'.format(stock_json.status_code, store['name']), json_url)
          continue
        stock_num = int(stock_json.json()['StockAvailability']['RetailItemAvailability']['AvailableStock']['$'])
        all_stores[store['value']]['stock'][product[0]] = stock_num

feature_collection = geojson.FeatureCollection([])

print('='*100)
for store in all_stores.values():
  for product in store['stock']:
    record_stats('products', product, store['stock'][product])
  print(store['name'], store['stock'])
  feature_collection.features.append(
    geojson.Feature(
      geometry=geojson.Point((
        float(store['lng']),
        float(store['lat'])
      )),
      properties={
        'name': store['name'],
        'stock': store['stock']
      }
    )
  )

print('='*100)
print('Stats:')
pprint.pprint(stats)
print('='*100)

if 'products' not in stats:
  print('haven\'t found anything at all, exiting')
  sys.exit(42)

geojson.dump(feature_collection, open('public/blahaj.geojson', 'w'))

log = open('public/log.txt', 'a')
log.write(str(datetime.datetime.now())+" -- Python "+str(platform.python_version())+" -- "+str(platform.uname())+'\n')

